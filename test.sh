#!/bin/bash

mkdir -p build/test

compiler=${1:-clang++}
${compiler} -std=c++17 ./test/example.cpp -o build/test/example -I ./lib -Wall -Wextra -Wpedantic
./build/test/example

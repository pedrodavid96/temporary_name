#include <ft2build.h>
#include FT_FREETYPE_H

namespace fonts {

class FontLibrary;
class FontFace;

class FontChar {
    private:
    friend class FontFace;

    static const FT_GlyphSlot createGlyph(const FT_Face face, const FT_ULong charCode) {
        if (FT_Load_Char(face, charCode, FT_LOAD_RENDER)) {
            std::cout << "ERROR::FREETYTPE: Failed to load Glyph\n";
        }
        return face->glyph;
    }
    public:
    const FT_GlyphSlot glyph;
    private:
    FontChar(const FT_Face face, const FT_ULong charCode) : glyph { createGlyph(face, charCode) } { }
};

class FontFace {
    private:
    friend class FontLibrary;

    static const FT_Face createFtFace(const FT_Library library, const std::string_view res) {
        FT_Face face;
        if (FT_New_Face(library, res.data(), 0, &face)) {
            std::cout << "ERROR::FREETYPE: Unsupported type\n";
        }
        return face;
    }
    const FT_Face face;
    FontFace(const FT_Library library, const std::string_view res, const FT_UInt height) : face { createFtFace(library, res) } {
        if (FT_Set_Pixel_Sizes(face, 0, height)) {
            std::cout << "ERROR::FREETYPE: Invalid pixel size, check face-fixed_sizes\n";
        }
    }

    public:
    inline ~FontFace() {
        FT_Done_Face(face);
    }
    const FontChar getChar(const FT_ULong charCode) {
        return FontChar { face, charCode };
    }
};

class FontLibrary {
    private:
    static const FT_Library createFtLibrary() {
        FT_Library ft;
        if (FT_Init_FreeType(&ft)) {
            std::cout << "ERROR::FREETYPE: Could not init FreeType Library\n";
        }
        FT_Init_FreeType(&ft);
        return ft;
    }
    const FT_Library ft;

    public:
    FontLibrary() : ft { createFtLibrary() } { }
    inline ~FontLibrary() {
        FT_Done_FreeType(ft);
    }

    const FontFace createFace(const std::string_view res, const FT_UInt size) const {
        return FontFace{ ft, res, size };
    }
};
}
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#pragma warning(push)
#include <codeanalysis\warnings.h>
#pragma warning(disable: ALL_CODE_ANALYSIS_WARNINGS)
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#pragma warning(pop)

#ifdef GLAD_DEBUG
#include <stdarg.h>
void pre_gl_call(const char* name, void*, int len_args, ...)
{
    printf("[GL] %s (%d arguments)\n", name, len_args);
}
#endif

namespace graphics {
    class WindowContext;
    class Window {
        private:
        friend WindowContext;
        struct GLFWwindowDeleter {
            void operator()(GLFWwindow* window) {
                glfwDestroyWindow(window);
            }
        };
        private:
        const std::unique_ptr<GLFWwindow, GLFWwindowDeleter> window;
        Window(const std::string_view title, const size_t width, const size_t height) :
            window { glfwCreateWindow(static_cast<int>(width), static_cast<int>(height), title.data(), NULL, NULL) }
        {
            if (window.get() == nullptr) {
                throw "Failed to Initialize Window";
            }
            glfwMakeContextCurrent(window.get());
            if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
                throw "Failed to Load GL context";
            }
        }

        template<typename State>
        static inline State* getState(GLFWwindow* window) {
            return static_cast<State*>(glfwGetWindowUserPointer(window));
        }

        inline void setState(void *state) const {
            glfwSetWindowUserPointer(window.get(), state);
        }

        public:
        inline void close() const {
            glfwSetWindowShouldClose(window.get(), GLFW_TRUE);
        }

        inline bool shouldClose() const {
            return glfwWindowShouldClose(window.get());
        }

        inline void update() const {
            glfwSwapBuffers(window.get());
            // TODO: This should maybe go to window context 
            // counter point: window context would need to know which window to call)
            glfwPollEvents();
        }

        /**
         * Warning: This method overrides the Window State (WindowUserPointer)
         */
        template<typename Callback>
        inline void setKeyCallback(Callback& callback) const {
            setState(&callback);
            glfwSetKeyCallback(window.get(), [](GLFWwindow* window, int key, int scancode, int action, int mods) {
                auto callback = getState<Callback>(window);
                (*callback)(key, scancode, action, mods);
            });
        }

        template<typename Callback>
        inline void setKeyCallback(Callback&& callback) const {
            static_assert(false, "Can't set Key Callback to RValue because it is a temporary and its pointer can't be stored (UB)");
        }
    };
    class WindowContext {
        public:
        WindowContext() {
            if (!glfwInit()) {
                throw "Failed to init GLFW";
            }
        }
        ~WindowContext() {
            glfwTerminate();
        }
        const Window make_window(const std::string_view title, const size_t width, const size_t height) const {
            return { title, width, height };
        }
    };

    class ShaderProgram;
    class Shader {
        private:
        friend class ShaderProgram;
        const GLenum id;
        public:
        Shader(const int type, const std::string_view shaderSource) :
            id { glCreateShader(type) }
        {
			auto c_src = shaderSource.data();
			glShaderSource(id, 1, &c_src, NULL);
			glCompileShader(id);

			GLint status;
			glGetShaderiv(id, GL_COMPILE_STATUS, &status);
			if (status == GL_FALSE) {
				GLint length;
				glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
				auto log = std::make_unique<GLchar[]>(length);
				glGetShaderInfoLog(id, length, &length, log.get());
				throw log.get();
			}
        }
        Shader(const Shader& shader) = delete;
        Shader(Shader&& shader) = delete;
        inline ~Shader() {
            glDeleteShader(id);
        }
    };

    class ShaderProgram {
        private:
        const GLuint id;
        public:
        template<typename ...Shaders>
        ShaderProgram(const Shaders&& ...shaders) :
            id { glCreateProgram() }
        {
			static_assert((std::is_base_of_v<Shader, Shaders> && ...), "ShaderProgram: one or more arguments is not a Shader" __FUNCSIG__);
            (glAttachShader(id, shaders.id), ...);
            glLinkProgram(id);
            glValidateProgram(id);
        }
        inline ~ShaderProgram() {
            glDeleteProgram(id);
        }
        inline void enable() const {
            glUseProgram(id);
        }
        inline void disable() const {
            glUseProgram(0);
        }
        inline GLuint getUniformLocation(const GLchar* name) const {
            return glGetUniformLocation(id, name);
        }
        inline void uniform3f(const GLuint location, const GLfloat x, const GLfloat y, const GLfloat z) const {
            glUniform3f(location, x, y, z);
        }
        inline void uniformMatrix4fv(const GLuint location, const GLsizei count, const GLboolean transpose, const GLfloat *value) const {
            glUniformMatrix4fv(location, count, transpose, value);
        }
    };

    namespace TGLenum {
        template<typename T>
        constexpr GLenum of() {
            //TODO: This could later become a lambda
            // Not working right now in MSVC (walks to if and then else anyway and fails)
            // Related: https://developercommunity.visualstudio.com/content/problem/200017/if-constexpr-in-lambda.html
            if constexpr(std::is_same_v<T, float>) {
                return GL_FLOAT;
            } else {
                static_assert(false, "No available conversion from type T to a GLenum");
            }
        }
    }

    template<typename T>
    constexpr GLenum GLenum_t = TGLenum::of<T>();

    template<typename T>
    struct base_type {
        using value = T;
    };

    template<typename T>
    using base_type_v = typename base_type<T>::value;
}

namespace behavior {

struct vao {
    const GLuint create() {
        GLuint vao;
        glCreateVertexArrays(1, &vao);
        return vao;
    }
    void free(const GLuint id) {
        glDeleteVertexArrays(1, &id);
    }
    void bind(const GLuint id) const {
        glBindVertexArray(id);
    }
};

GLuint createVBO();

struct vbo {
    const GLuint create() {
        return createVBO();
    }
    void free(const GLuint id) {
        glDeleteBuffers(1, &id);
    }
    void bind(const GLuint id) const {
        glBindBuffer(GL_ARRAY_BUFFER, id);
    }
};

struct ebo {
    const GLuint create() {
        return createVBO();
    }
    void free(const GLuint id) {
        glDeleteBuffers(1, &id);
    }
    void bind(const GLuint id) const {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
    }
};

GLuint createVBO() {
    GLuint vbo;
    glGenBuffers(1, &vbo);
    return vbo;
}

struct texture {
    const GLuint create() {
        GLuint texture;
        glGenTextures(1, &texture);
        return texture;
    }
    void free(const GLuint id) {
        glDeleteTextures(1, &id);
    }
    void bind(const GLuint id) const {
        glBindTexture(GL_TEXTURE_2D, id);
    }
};

}

template<typename TBehavior>
class Unique : TBehavior {
private:
    GLuint id;

public:
    Unique() : Unique { TBehavior::create() } { }
    Unique(const GLuint id) : id{ id } { bind(); }
    ~Unique() { TBehavior::free(id); }

    Unique(const Unique&) = delete;
    Unique(Unique&& rhs) : id{ std::move(rhs.id) } {
        rhs.reset();
    }

    Unique& operator=(const Unique&) = delete;

    void bind() const { TBehavior::bind(id); }
    void unbind() const { TBehavior::bind(0); }

protected:
    GLuint getId() const { return id; }
    void reset() { this->id = 0; }
};

namespace graphics {

struct VAO : Unique<behavior::vao> {
    const VAO unwrap() const {
        struct UnmanagedVAO : VAO {
            // TODO: Find workaround for this
            // Since the destructor is not virtual (because making it virtual
            // incures in a 4x memory penalty) it is not called when destroying
            // a VAO*, and as such, the resource is removed right away
            ~UnmanagedVAO() { Unique::reset(); }
        };
        return UnmanagedVAO{getId()};
    }
};

struct VBO : Unique<behavior::vbo> {
private:
    template<typename T>
    static void addAttribute(const GLuint attribute, const GLuint elementNumber, const std::size_t stride, const std::size_t offset) {
        glVertexAttribPointer(attribute, elementNumber, graphics::GLenum_t<T>, GL_FALSE, static_cast<GLsizei>(stride * sizeof(T)), (GLvoid*)(offset * sizeof(T)));
        glEnableVertexAttribArray(attribute);
    }

public:
    VBO(const GLuint id) : Unique { id } { }
    template<typename Container, typename ...AttribElements>
    VBO(const Container& vertices, const AttribElements&& ...attribElements) {
        using T = typename Container::value_type;
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(T), vertices.data(), GL_STATIC_DRAW);

        const std::size_t stride = (attribElements + ...);
        std::size_t i = 0;
        std::size_t offset = 0;
        ([attribElements, stride, &i, &offset](){
            addAttribute<graphics::base_type_v<T>>(static_cast<GLuint>(i), attribElements, stride, offset);
            offset += attribElements;
            i++;
        }(), ...);
    }
    // TODO: Improve next API
    template<typename T, std::size_t N>
    static VBO dynamic(const GLuint attribute, const unsigned int elementNumber) {
        GLuint id = behavior::createVBO();
        glBindBuffer(GL_ARRAY_BUFFER, id);

        glBufferData(GL_ARRAY_BUFFER, sizeof(T) * elementNumber * N, NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(attribute, elementNumber, GL_FLOAT, GL_FALSE, elementNumber * sizeof(T), 0);
        glEnableVertexAttribArray(attribute);
        return VBO{id};
    }
    void subData(const GLuint offset, GLsizeiptr size, const GLvoid *data) const {
        bind();
        glBufferSubData(GL_ARRAY_BUFFER, offset, size, data);
    }
};

struct EBO : public Unique<behavior::ebo> {
    EBO() = delete;
    template<typename Container>
    EBO(const Container& indices) {
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(Container::value_type), indices.data(), GL_STATIC_DRAW);
    }
};

struct Texture : Unique<behavior::texture> {
public:
    Texture(const std::string_view path) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
   
        int width, height, nrChannels;
        stbi_uc* data = stbi_load(path.data(), &width, &height, &nrChannels, 0);
    
        //TODO: handle nullptr

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
  
        stbi_image_free(data);
    }
    Texture(const FT_GlyphSlot glyph) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
        const FT_Bitmap bitmap = glyph->bitmap;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, bitmap.width, bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, bitmap.buffer);
    }
};

}
